> NOTE: You, not the author of HamPi, are responsible for making backups and otherwise keeping your existing files safe. These instructions are provided for convenience only, and hopefully do not cause you, the end user, issues. (These instructions have only been briefly tested.)


1. Backup Pi Home Directory using the following command:
    ```bash
    cd /tmp && \
    tar -jcvf pi_home_dir.tar.bz2 \
    --exclude-caches --exclude "pi/Downloads/*" \
    --exclude "pi/GridTracker/*" \
    --exclude "pi/HamRadio/*" \
    --exclude "pi/YAAC/*" \
    --exclude "pi/dump1090_1.3.1_sdrplay/*" \
    --exclude "pi/wsjtx_to_n3fjp/*" \
    --exclude "pi/pyqso-1.1.0/*" \
    --exclude "pi/Documents/GridTracker/*" \
    --exclude "pi/Documents/ardop-list/*" \
    --exclude "pi/.local/lib/python3.7/*" \
    --exclude "pi/perl5/*" \
    --exclude "pi/ardop/*" \
    --absolute-names \
    --preserve-permissions \
    /home/pi/
    ```
1. Save your backup somewhere off the Pi
    - Then copy the file `pi_home_dir.tar.bz2` on to external media, for example, a USB memory stick.
1. Flash the new HamPi image to a new (micro)SD card.
1. Copy the file `pi_home_dir.tar.bz2` to your `/tmp` directory on the new HamPi card.
1. This is optional. If you want to see what files will be replaced on your new HamPi card, you may run Tar in a test or dry run mode:
    ```bash
    cd /tmp && tar -jtvf pi_home_dir.tar.bz2 --skip-old-files
    ```
1. Restore Pi Home Directory using the following command (it is all one line):
    ```bash
    cd /tmp && tar -jxvf pi_home_dir.tar.bz2 --skip-old-files
    ``` 

