> This repo is a fork of the HamPi project on GitHub. I am **not** the creator and am only hosting for access to the resource along with some improvements.
>
> 73 W7CLI

<img src="logo.png" alt="HamPi Logo" width="400px" />

Fellow Hams and Raspberry Pi Enthusiasts,

I am pleased to announce the release of HamPi 1.0 – The Raspberry Pi Ham Radio Image I've been working on for the last six months (Dec. 2019 – June 2020).

I have confirmed this image starts up successfully on the Raspberry Pi Model 4 B. Compatibility is currently being run on earlier Raspberry Pi models. A series of beta-testers have tested this and provided feedback prior to release, and I have used that feedback to make this a better image for everyone.

The primary place to download this is via BitTorrent. 

* Magnet Link: <a href="magnet:?dn=HamPi+v1.0+by+W3DJS&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&xl=2097152&xt=urn:btih:WAGBRSYNDMJNATBQSN4P5AHZUTUZE6DO"><img src="magnet.png" alt="Click to open torrent magnet link..." title="Click to open torrent magent link..." width="20px"  /></a>
* Torrent File: [HamPi_v1.torrent](HamPi_v1.torrent)

If you can’t, or won’t, use BitTorrent, there are a number of direct download links:

* US Callsign Alpha Prefix download here: https://bit.ly/HamPi_1_0-W3DJS
* US Callsign Kilo Prefix download here: https://bit.ly/HamPi_1_0-WA4ZXV
* US Callsign November Prefix download here: https://bit.ly/HamPi_1_0-N5XMT
* US Callsign Whiskey Prefix download here: https://bit.ly/HamPi_1_0-KE8OHG
* All other Prefixes download here: https://bit.ly/HamPi_1_0-VE4VR

For hosting direct download sites, thank you to: Norm Schklar, WA4ZXV, William Franzin, VE4VR, David Cooley, N5XMT, and David Potter, KE8OHG.

Specific to this repo on GitLab, if cloning, you will need to download the lfs extention to git. This is how the HamPi image is being stored. Info here: https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/
Once installed you'd just checkout as normal:
```bash
git clone git@gitlab.com:CompSciMaj13/HamPi.git
```

If you've already cloned the repo and want the most recent image, run:
```bash
git lfs fetch origin master
```
More info on git lfs: https://docs.gitlab.com/ee/topics/git/lfs/index.html

A BIG thank you to my XYL, Michelle, for creating the HamPi Logo!

Please send me any comments or concerns via email to w3djs@arrl.net.

-Dave Slotter, W3DJS


> **ATTENTION:** So I don’t get publicly tarred and feathered for not disclosing this openly, please be aware by running this image on your Raspberry Pi that you consent to giving your idle cpu cycles to radio telescope research, such as looking for pulsars. If you do not agree with this, do not run the image. Again, this will not interfere with your usage – this only uses cpu cycles when your Raspberry Pi is idle. Disk use is also minimal. (1/1000 or less)
(For what it’s worth, I also operate my Raspberry Pi 4 this way.)

# HamPi 1.0 – The Raspberry Pi Ham Radio Image by W3DJS


## General Ham Radio Applications
* [HamLib](https://hamlib.github.io/) - Ham Radio Control Libraries
* [grig](http://sourceforge.net/projects/groundstation) - graphical user interface to the Ham Radio Control Libraries
* [CHIRP](https://chirp.danplanet.com/) - Radio Programming Software
* [APRS](http://m0iax.com/2019/09/25/aprs-message-app-for-js8call/) Message App for JS8Call - GUI to send APRS messages via JS8Call
* [QTel](https://www.svxlink.org/) - EchoLink client
* [QSSTV](http://users.telenet.be/on4qz/index.html) - Slow Scan TV (e.g. "Fax")
* [Gpredict](http://gpredict.oz9aec.net/) - Satellite prediction
* [FreeDV](https://freedv.org/) - Free digital voice vocoder
* [BlueDV](https://www.pa7lim.nl/bluedv/) - Client for D-Star and DMR
* [WsprryPi](https://github.com/8cH9azbsFifZ/WsprryPi.git) - WSPR software
* [ADS-B Flight Tracking Software](https://github.com/MalcolmRobb/dump1090)
* [Pi3/4 Stats Monitor](http://www.w1hkj.com/pi3/) - by [W1HKJ](http://www.w1hkj.com/)
* [VOACAP](https://www.qsl.net/hz1jw/voacapl/index.html) - HF propagation prediction
* [GPS Support](https://gpsd.gitlab.io/gpsd/)
* [Auto WiFi Hotspot](https://www.raspberryconnect.com/projects/65-raspberrypi-hotspot-accesspoints/158-raspberry-pi-auto-wifi-hotspot-switch-direct-connection) - Automatically turn your Pi into a WiFi hotspot when in the field!
* [wxtoimg](https://wxtoimgrestored.xyz/beta/) - NOAA weather imaging software
* [twHamQTH](http://wa0eir.bcts.info/twhamqth.html) - an online callsign look up program
* [twclock](http://wa0eir.bcts.info/twclock.html) - a world clock and automatic ID for amateur radio operators
* [acfax](https://www.qsl.net/dl4sdc/) - Receive faxes using your radio and sound card
* [colrconv](http://db0fhn.efi.fh-nuernberg.de/doku.php?id=radio:db0fhn:convers)  -  convers client with sound and ncurses color support
* [D-Rats 0.3.9 (by new maintainer Maurizio Andreotti)](https://github.com/maurizioandreotti/D-Rats) - A communication tool for D-STAR
* [fbb](http://www.f6fbb.org/) - Packet radio mailbox and utilities
* gcb - Utility to calculate long and short path to a location
* [glfer](https://www.qsl.net/in3otd/) - Spectrogram display and QRSS keyer
* [Xdx](https://github.com/N0NB/xdx) is a DX-cluster client
* [DXSpider](http://www.dxcluster.org/main/) - DX Cluster Server
* [fccexam](https://www.qrz.com/db/AC6SL) - Study tool for USA FCC commercial radio license exams.
* [gnuais / gnuaisgui](https://github.com/rubund/gnuais/) - GNU Automatic Identification System receiver
* [hamexam](https://www.qrz.com/db/AC6SL) - Study guide for USA FCC amateur radio (ham radio) license examinations.
* [hamfax](http://hamfax.sourceforge.net/) - Qt based shortwave fax
* [inspectrum](https://github.com/miek/inspectrum) - tool for visualising captured radio signals
* [predict-gsat](https://github.com/kd2bd/predict/) - Graphical Predict client
* [splat](http://www.qsl.net/kd2bd/splat.html) - analyze point-to-point terrestrial RF communication links
* [wwl](https://www.qrz.com/db/VA3DB) - Calculates distance and azimuth between two Maidenhead locators
* [AX.25](https://github.com/ve7fet/linuxax25) – Packet Radio drivers for ax.25 protocol
* [linpac](https://sourceforge.net/projects/linpac) - terminal for packet radio with mail client
* [PyBOMBS](https://github.com/gnuradio/pybombs) - GNU Radio install management system
* [AMBEServer](https://github.com/marrold/AMBEServer) – AMBE vocoder chip support


## Antenna Ham Radio Applications
* [antennavis](http://www.include.gr/antennavis.html) - Antenna Visualization Software
* [gsmc](https://github.com/radioteknos/gsmc.git) - A GTK Smith Chart Calculator for RF impedance matching
* [nec2c](https://www.qsl.net/5b4az/) - Translation of the NEC2 FORTRAN source code to the C language
* [xnecview](http://www.pa3fwm.nl/software/xnecview/) - NEC structure and gain pattern viewer
* [yagiuda](http://www.pa3fwm.nl/software/xnecview/) - software to analyse performance of Yagi-Uda antennas


## Digital Mode Ham Radio Applications
* [WSJT-X](https://physics.princeton.edu/pulsar/k1jt/wsjtx.html) - Weak Signal (FT8, FT4, etc.) by [W1JT](https://www.qrz.com/db/K1JT)
* [GridTracker](https://tagloomis.com/grid-tracker/) - Graphical mapping companion program for WSJT-X or JTDX
* [JTDX](http://www.jtdx.tech/en/) - Alternate client for Weak Signal (FT8, FT4, etc.)
* [JS8Call](http://js8call.com/) - Messaging built on top of FT8 protocol by [KN4CRD](https://www.qrz.com/db/kn4crd/)
* [JS8CallTools](http://m0iax.com/2019/05/27/js8calltools-for-raspberry-pi/) - Get Grid coordinates using GPS
* (FLDigi is in its own section below.)
* [gnss-sdr](https://github.com/gnss-sdr/gnss-sdr) - GLONASS satellite system Software Defined Receiver
* [linpsk](http://linpsk.sourceforge.net/) - amateur radio PSK31/RTTY program via soundcard
* [multimon](https://sourceforge.net/projects/multimon/) - multimon - program to decode radio transmissions
* [multimon-ng](https://github.com/EliasOenal/multimon-ng) - digital radio transmission decoder
* [psk31lx](http://wa0eir.bcts.info/psk31lx.html) - a terminal based ncurses program for psk31
* [twpsk](http://wa0eir.bcts.info/twpsk.html) - a psk program


## Software Defined Radio
* [CubicSDR](https://cubicsdr.com/) - Software Defined Radio receiver
* [cutesdr](https://sourceforge.net/projects/cutesdr/) - Simple demodulation and spectrum display program
* [GQRX](http://gqrx.dk/) - Software defined radio receiver
* [SDRAngel](https://github.com/f4exb/sdrangel) - SDR player
* [lysdr](https://github.com/gordonjcp/lysdr) - Simple software-defined radio
* [SoapyAudio](https://github.com/pothosware/SoapyAudio) - Soapy SDR plugin for Audio devices
* [SoapyHackRF](https://github.com/pothosware/SoapyHackRF) - SoapySDR HackRF module
* [SoapyMultiSDR](https://github.com/pothosware/SoapyMultiSDR) - Multi-device support module for SoapySDR
* [SoapyNetSDR](https://github.com/pothosware/SoapyNetSDR) - Soapy SDR module for NetSDR protocol
* [SoapyRemote](https://github.com/pothosware/SoapyRemote) - Use any Soapy SDR remotely
* [SoapyRTLSDR](https://github.com/pothosware/SoapyRTLSDR) - Soapy SDR module for RTL SDR USB dongle
* [SoapySDR](https://github.com/pothosware/SoapySDR) - Vendor and platform neutral SDR support library
* [SoapySDRPlay](https://github.com/pothosware/SoapySDRPlay) - Soapy SDR module for SDRPlay
* Support for [RTL-SDR](https://www.rtl-sdr.com/)
* Support for [SDRPlay SDR](https://www.sdrplay.com/)
* Support for [HackRF SDR](https://greatscottgadgets.com/hackrf/)
* Support for [AirSpy](https://github.com/airspy/airspyone_host) and [AirSpy HF](https://github.com/airspy/airspyhf)
* [SoapySDRAirSpy](https://github.com/pothosware/SoapyAirspy) - Soapy SDR module for AirSpy SDR
* [SoapySDRFUNcube Dongle Pro+](https://github.com/pothosware/SoapyFCDPP) - Soapy SDR module for FUNCube Dongle Pro+
* [SoapySDRPlutoSDR](https://github.com/pothosware/SoapyPlutoSDR) - Soapy SDR module for Pluto SDR
* [SoapySDROsmoSDR](https://github.com/pothosware/SoapyOsmo) - Soapy SDR module for Osmo SDR
* [SoapySDRRedPitaya](https://github.com/pothosware/SoapyRedPitaya) - Soapy SDR module for Red Pitaya SDR
* [SoapyUHD](https://github.com/pothosware/SoapyUHD) - Soapy SDR module for Ettus ResearchUHD SDR
* [SoapySDRVOLKConverters](https://github.com/pothosware/SoapyVOLKConverters) - Support for VOLK-based type converters


## APRS Applications
* [Xastir](http://xastir.org/) - APRS GUI client / Digipeater / Igate
* [YAAC](https://www.ka2ddo.org/ka2ddo/YAAC.html) - Yet Another APRS Client
* [DireWolf](https://github.com/wb2osz/direwolf) - Software "soundcard" AX.25 packet modem/TNC and APRS encoder/decoder
* [aprsdigi](https://github.com/n2ygk/aprsdigi) - digipeater for APRS
* [aprx](https://thelifeofkenneth.com/aprx/) - APRS Digipeater and iGate
* [soundmodem](http://soundmodem.vk4msl.id.au/) - Sound Card Amateur Packet Radio Modems


## FLDigi Application Suite from [W1HKJ](http://www.w1hkj.com/)
* [flrig](http://www.w1hkj.com/) - Rig Control program which interfaces with fldigi
* [fldigi](http://www.w1hkj.com/) - [Digital Modes](http://www.w1hkj.com/modes/index.htm) Communications
* [flaa](http://www.w1hkj.com/) - RigExpert Antenna Analyzer Control Program
* [flamp](http://www.w1hkj.com/) - File transmissions via Amateur Multicast Protocol
* [flarq](http://www.w1hkj.com/) - ARQ data transfer utility for fldigi
* [flcluster](http://www.w1hkj.com/) - Telnet client to remote DX Cluster Servers
* [fllog](http://www.w1hkj.com/) - Logbook application which can use same data file as fldigi
* [flmsg](http://www.w1hkj.com/) - Editor for ICS 213 Forms
* [flnet](http://www.w1hkj.com/) - Net Control Assistant for Net Activities (Check-In Application)
* [flpost](http://www.w1hkj.com/) - NBEMs post office
* [flwrap](http://www.w1hkj.com/) - File encapsulation and compression for transmission over amateur radio
* [flwkey](http://www.w1hkj.com/) - Winkeyer (or clone) control program for K1EL Winkeyer series


## Logging Applications
* [TrustedQSL](http://www.arrl.org/tqsl-download) - LotW client
* [CQRlog](https://www.cqrlog.com/) - Ham Radio Logging Application
* [PyQSO](https://christianjacobs.uk/pyqso/) - Logging software (written in Python)
* [klog](https://www.klog.xyz/) - The Ham Radio Logging program
* [tlf](https://tlf.github.io/) - console based ham radio contest logger
* [tucnak2](ihttp://tucnak.nagano.cz/wiki/Main_Page) - VHF/UHF/SHF Hamradio contest log version 2
* [twlog](http://wa0eir.bcts.info/twlog.html) - basic logging program for ham radio
* upload_adif_log – Upload only new log entries to LotW, eQSL.cc and ClubLog
* [wsjtx_to_n3fjp](https://github.com/dslotter/wsjtx_to_n3fjp) - Logging adapter to allow WSJT-X to log to N3FJP
* [xlog](https://www.nongnu.org/xlog/) - GTK+ Logging program for Hamradio Operators


## WinLink Applications
* [Pat WinLink](https://getpat.io/) - WinLink for Raspberry Pi (and other platforms)
* [ARDOP](http://www.cantab.net/users/john.wiseman/Documents/ARDOPC.html) support for Pat WinLink
* [ARDOP-GUI](https://www.cantab.net/users/john.wiseman/Downloads/Beta/) - Provides graphical representation of ARDOP connections
* [Find ARDOP](https://app.simplenote.com/publish/LR0lxm) - Retrieves local ARDOP sources by [KM4ACK](https://www.qrz.com/db/KM4ACK)
* Pat Menu 2 – Menu for Pat by KM4ACK
* [PMON](https://www.p4dragon.com/en/PMON.html) - a PACTOR® Monitoring Utility for Linux


## Morse Code Applications
* [aldo](https://www.nongnu.org/aldo/) - Morse code training program
* [cw](http://unixcw.sourceforge.net/about.html) - sound characters as Morse code on the soundcard or console speaker
* [cwcp](http://unixcw.sourceforge.net/) - Text based Morse tutor program
* [xcwcp](http://unixcw.sourceforge.net/) - Graphical Morse tutor program
* [cwdaemon](http://cwdaemon.sourceforge.net/) - morse daemon for the serial or parallel port
* [ebook2cw](https://fkurz.net/ham/ebook2cw.html) - convert ebooks to Morse MP3s/OGGs
* [ebook2cwgui](http://fkurz.net/ham/ebook2cw.html) - GUI for ebook2cw
* [morse](http://www.catb.org/~esr/morse-classic/) - training program about morse-code for aspiring radio hams
* [morse2ascii](http://aluigi.altervista.org/mytoolz.htm) - tool for decoding the morse codes from a PCM WAV file
* [morsegen](http://aluigi.altervista.org/mytoolz.htm) - convert file to ASCII morse code
* [qrq](https://fkurz.net/ham/qrq.html) - High speed Morse telegraphy trainer
* [xdemorse](https://launchpad.net/ubuntu/+source/xdemorse/3.4-1) - decode Morse signals to text


*** Ham Radio Wallpaper also included in image ***

