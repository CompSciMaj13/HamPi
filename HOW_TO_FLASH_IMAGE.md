No matter if you are a Windows, Mac os Linux user, go to https://www.balena.io/etcher/ and
download and install Balena Etcher. This is the easiest program I can suggest to use.

1. Start Balena Etcher.
1. Click on Select Image and select the downloaded `HamPi_v1.0.img.xz` file.
    - (If you got HamPi in a tar file, you'll need to extract the files from the tar archive first.)
1. Click on Select Drive and select your 16GB or larger (micro)SD card.
1. Click on Flash!

After several minutes, the flashing process will finish, and you can eject the (micro)SD card
and insert it into your Raspberry Pi and power it up.
